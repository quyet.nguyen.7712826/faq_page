import axios from "axios";
export default axios.create({
  baseURL: "https://b92e-113-190-234-128.ngrok.io/api",
  headers: {
    "Content-type": "application/json"
  }
});
