import { createApp } from 'vue'
import App from './App.vue'

createApp(App).mount('#vify-easy-and-simple-faq-app')
