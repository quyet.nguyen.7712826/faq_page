import http from "../http-common";
class SettingDataService {

    getAll(user_id) {
        return http.get(`/setting/user/${user_id}`);
    }
    findByTitle(title) {
        return http.get(`/setting?title=${title}`);
    }
}
export default new SettingDataService();
